import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class Main {
    public static void main(String[] args) {
        int[] primeNumber = {2, 3, 5, 7, 11};
        String[] primeNumberString = {"first", "second", "third", "fourth", "fifth"};

        for(int i=0; i<primeNumber.length; i++){
            System.out.printf("The %s prime number is: %d%n", primeNumberString[i], primeNumber[i]);
        }

        ArrayList<String> friends = new ArrayList<String>();

        friends.add("John");
        friends.add("Jane");
        friends.add("Chloe");
        friends.add("Zoey");

        System.out.println("My friends are: " + friends);

        HashMap<String, Integer> inventory = new HashMap<String, Integer>(){
            {
                put("toothpaste", 15);
                put("toothbrush", 20);
                put("soap", 12);
            }
        };

        System.out.println("Our current inventory consists of: " + inventory);
    }
}